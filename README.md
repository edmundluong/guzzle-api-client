Guzzle API Client

Provides a simple wrapper around Guzzle Web Services 0.5, and facilitates the process of adding pre/post-hook events on a HTTP request.
Useful for adding special authentication to HTTP requests, which is a common task when accessing a website's REST API.

See documentation for Guzzle components:

* http://guzzle3.readthedocs.org/webservice-client/guzzle-service-descriptions.html