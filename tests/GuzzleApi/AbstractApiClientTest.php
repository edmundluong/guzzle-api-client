<?php

/**
 * This file is part of the guzzle-api-client package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   guzzle-api-client
 * @author    Edmund Luong <edmundvmluong@gmail.com>
 * @copyright Copyright (c) 2015, Edmund Luong
 */

namespace GuzzleApi;

use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Subscriber\Log\LogSubscriber;

/**
 * Tests the Twitter URLs API client, which tests the abstract API client.
 *
 * @package GuzzleApi
 * @version 1.0.0
 */
class AbstractApiClientTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function it_throws_a_malformed_api_description_exception_when_given_an_invalid_format()
    {
        $this->setExpectedException('\GuzzleApi\Exceptions\InvalidDescriptionException');
        new TwitterUrlsApiClient(['apiDescription' => 'foo']);
    }

    /** @test */
    public function it_can_be_switched_into_debug_mode_with_a_flag()
    {
        $client = new TwitterUrlsApiClient(['debug' => false]);
        $listeners = $client->getHttpClient()->getEmitter()->listeners();
        $this->assertEquals(0, count($listeners));

        $client = new TwitterUrlsApiClient(['debug' => true]);
        $listeners = $client->getHttpClient()->getEmitter()->listeners();
        $this->assertEquals(2, count($listeners));
        $this->assertEquals(true, $listeners['error'][0][0] instanceof LogSubscriber);
    }

    /** @test */
    public function it_makes_an_api_call_with_magic_methods()
    {
        $twitterUrls = new TwitterUrlsApiClient();

        $response = $twitterUrls->count(['url' => 'http://www.google.com']);

        $this->assertEquals(true, array_key_exists('count', $response));
        $this->assertEquals(true, array_key_exists('url', $response));
        $this->assertEquals('http://www.google.com/', $response['url']);
    }
}

/**
 * Twitter URLs API client for tests.
 *
 * @package GuzzleApi
 * @version 1.0.0
 */
class TwitterUrlsApiClient extends AbstractApiClient
{
    protected function configure(
        array $config = [],
        array $default = [],
        array $required = []
    ) {
        $default = ['apiDescription' => TwitterUrlsApiDescription::load()];

        return parent::configure($config, $default, $required);
    }
}

/**
 * Google Location API description for tests.
 * Uses Google Location Public API for testing purposes.
 *
 * The API endpoint is:
 * https://maps.googleapis.com/maps/api/geocode/json?address=Oxford%20University,%20uk&sensor=false
 *
 * @package GuzzleApi
 */
class TwitterUrlsApiDescription extends AbstractApiDescription
{
    public static function load()
    {
        return new Description([
            'additionalProperties' => true,
            'baseUrl'              => 'http://urls.api.twitter.com/1/urls/',
            'operations'           => [
                'count' => [
                    'httpMethod'    => 'GET',
                    'uri'           => 'count.json',
                    'responseModel' => 'JsonResponse',
                    'parameters'    => [
                        'url' => [
                            'type'     => 'string',
                            'location' => 'query',
                            'required' => true
                        ]
                    ]
                ]
            ],
            'models'               => [
                'JsonResponse' => [
                    'type'                 => 'object',
                    'additionalProperties' => [
                        'location' => 'json'
                    ]
                ]
            ]
        ]);
    }
}
