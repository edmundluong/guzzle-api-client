<?php

/**
 * This file is part of the Localbitcoins PHP Client package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author    Edmund Luong <edmundvmluong@gmail.com>
 * @copyright Copyright (c) 2015, Edmund Luong
 * @link      https://github.com/edmundluong/localbitcoins-php-client
 */

namespace GuzzleApi;

use GuzzleApi\Exceptions\InvalidDescriptionException;
use GuzzleApi\Utils\ConfigurableTrait;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Collection;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Subscriber\Log\Formatter;
use GuzzleHttp\Subscriber\Log\LogSubscriber;

/**
 * Abstract API client.
 *
 * @author Edmund Luong <edmundvmluong@gmail.com>
 */
abstract class AbstractApiClient extends GuzzleClient
{
    use ConfigurableTrait;

    /**
     * @var Description $apiDescription
     */
    protected $apiDescription = null;

    /**
     * @var Client $client
     */
    protected $client = null;

    /**
     * @var  array $config
     */
    protected $config = [];

    /**
     * Instantiates an abstract API client.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct(
            $this->initialize($this->configure($config)),
            $this->apiDescription
        );
    }

    /**
     * Initializes the abstract API client.
     *
     * @param array $config
     *
     * @return ClientInterface
     */
    protected function initialize(array $config = [])
    {
        $client = new Client($config);
        $this->setApiDescription($config);
        $this->setDebugger($client, $config);

        return $this->client = $client;
    }

    /**
     * Sets the API service description.
     *
     * @param array $config
     *
     * @throws InvalidDescriptionException
     */
    private function setApiDescription(array $config)
    {
        if (
            !$this->configurationExists($config, 'apiDescription')
            || !$this->isValidDescription($config['apiDescription'])
        ) {
            $error = (is_null($this->apiDescription))
                ? 'No API service description found'
                : 'Malformed API service description';

            throw new InvalidDescriptionException($error);
        }

        $this->apiDescription = $config['apiDescription'];
    }

    /**
     * Determines whether an object is an API service description.
     *
     * @param mixed $description
     *
     * @return bool
     */
    private function isValidDescription($description)
    {
        return $description instanceof Description;
    }

    /**
     * Sets the API client debugger.
     *
     * @param ClientInterface $client
     * @param array $config
     */
    private function setDebugger(ClientInterface $client, array $config)
    {
        if ($this->isInDebugMode($config['debug'])) {
            $client->getEmitter()->attach(
                new LogSubscriber(null, Formatter::DEBUG)
            );
        }
    }

    /**
     * Whether the API client is in debug mode.
     *
     * @param mixed $mode
     *
     * @return  bool
     */
    private function isInDebugMode($mode)
    {
        return $mode === true;
    }

    /**
     * Configures the abstract API client.
     *
     * @param array $config
     * @param array $default
     * @param array $required
     *
     * @return array
     */
    protected function configure(
        array $config = [],
        array $default = [],
        array $required = []
    ) {
        $baseDefault = [
            'apiDescription' => null,
            'debug'          => false
        ];
        $baseRequired = [
            'apiDescription',
            'debug'
        ];

        return $this->config = Collection::fromConfig(
            $config,
            array_merge($baseDefault, $default),
            array_merge($baseRequired, $required)
        )->toArray();
    }
}