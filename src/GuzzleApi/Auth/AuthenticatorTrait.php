<?php

/**
 * This file is part of the Localbitcoins PHP Client package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author    Edmund Luong <edmundvmluong@gmail.com>
 * @copyright Copyright (c) 2015, Edmund Luong
 * @link      https://github.com/edmundluong/localbitcoins-php-client
 */

namespace GuzzleApi\Auth;

use GuzzleHttp\Event\BeforeEvent;
use GuzzleHttp\Event\RequestEvents;

/**
 * API authentication listener.
 *
 * @author Edmund Luong <edmundvmluong@gmail.com>
 */
trait AuthenticatorTrait
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array
     */
    public function getEvents()
    {
        return ['before' => ['sign', RequestEvents::SIGN_REQUEST]];
    }

    /**
     * Signs an API request using an authentication flow.
     *
     * @param BeforeEvent $event
     */
    abstract function sign(BeforeEvent $event);

    /**
     * Checks whether the request is authenticated with the specified type.
     *
     * @param BeforeEvent $event
     * @param string      $type
     *
     * @return bool
     */
    private function isAuthenticationType(BeforeEvent $event, $type)
    {
        return $event->getRequest()->getConfig()['auth'] === $type;
    }
}