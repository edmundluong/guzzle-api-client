<?php

/**
 * This file is part of the Localbitcoins PHP Client package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author    Edmund Luong <edmundvmluong@gmail.com>
 * @copyright Copyright (c) 2015, Edmund Luong
 * @link      https://github.com/edmundluong/localbitcoins-php-client
 */

namespace GuzzleApi\Exceptions;

use Exception;

/**
 * Invalid API client authorization exception.
 *
 * @author Edmund Luong <edmundvmluong@gmail.com>
 */
class ClientAuthException extends Exception
{
}