<?php

/**
 * This file is part of the Localbitcoins PHP Client package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author    Edmund Luong <edmundvmluong@gmail.com>
 * @copyright Copyright (c) 2015, Edmund Luong
 * @link      https://github.com/edmundluong/localbitcoins-php-client
 */

namespace GuzzleApi;

use BadFunctionCallException;
use GuzzleHttp\Command\Guzzle\Description;

/**
 * API service description interface.
 *
 * @author Edmund Luong <edmundvmluong@gmail.com>
 */
abstract class AbstractApiDescription
{
    /**
     * Loads the API service description.
     *
     * @return  Description
     */
    public static function load()
    {
        throw new BadFunctionCallException('Must override parent function');
    }
}